const { TopLayout, reduxStore } = require('./src/components/layout/TopLayout');
const { navigate } = require('gatsby');
const { log: consoleLog, readyNavigate } = require('./src/utils');

const log = (text, { location, prevLocation }) => consoleLog(text, location.pathname,
  'state:', location.state,
  'prev:', prevLocation && prevLocation.pathname);
const clientOnlyPath = /^\/user\/?$/;

exports.wrapPageElement = TopLayout;

exports.onPreRouteUpdate = async args => {
  const { location, prevLocation } = args;
  if (clientOnlyPath.test(location.pathname)) {
    // I had to create redirection here because of 2 reasons:
    // 1. Reach-Router Redirect component is breaking under Gatsby, don't know why
    // 2. Gatsby createRedirect API is probably incompatible with client-only pages
    consoleLog('redirecting...');
    await readyNavigate();
    navigate('/user/profile', {
      state: {
        saga: true
      },
      replace: true
    });
    return;
  }
  if (!prevLocation || (location.state && location.state.saga)) return;
  log('Gatsby started to change location to:', args);
  reduxStore.dispatch({ type: 'AUTH_WAIT', auth: { wait: true } });
}

exports.onRouteUpdate = args => {
  const { location, prevLocation } = args
  if (!prevLocation || clientOnlyPath.test(location.pathname) || (location.state && location.state.saga)) return;
  log('Gatsby finished changing location to:', args);
  reduxStore.dispatch({ type: 'AUTH_WAIT', auth: { wait: false } });
}
