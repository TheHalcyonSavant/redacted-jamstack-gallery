export default theme => ({
  card: {
    // width: 300,
    '& dt, & dd': {
      width: '50%',
      margin: '1rem 0',
      padding: '0 2rem',
      display: 'inline-block',
      float: 'left',
      verticalAlign: 'middle',
      lineHeight: 1.4,
      // whiteSpace: 'nowrap',
    },
    '& dt': {
      fontWeight: 'bold',
      clear: 'left',
    },
    '& img': {
      // display: 'block',
      // clear: 'both',
      maxWidth: 100,
      maxHeight: 100,
      margin: '0 auto',
    },
  }
});
