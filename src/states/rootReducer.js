const initialState = {
  auth: {
    loggedIn: false,
    user: {},
    userMenu: [],
    userRole: '',
    admin: {},
  },
  wait: true,
};

export default function rootReducer(state = initialState, action) {
  const { type, ...payload } = action;

  if (type === 'AUTH_CLEAR') return {
    ...state,
    auth: initialState.auth,
    wait: !!action.wait
  }
  return { ...state, ...payload }
}
