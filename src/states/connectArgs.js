export const actions = {
  logOut: () => ({ type: 'USER_SIGNOUT' }),
  // waitCountdown: () => ({ type: 'WAIT_COUNTDOWN' }),
}

export const states = state => ({
  loggedIn: state.auth.loggedIn,
  wait: state.auth.wait,
})
