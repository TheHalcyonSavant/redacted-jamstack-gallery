import { eventChannel } from 'redux-saga';
import { call, cancelled, fork, put, select, take } from 'redux-saga/effects';
import { navigate } from 'gatsby';
import { readyFirebase } from '../firebase';
import { log, readyNavigate } from '../utils';

const QUERY_OPTIONS = {
  page: 0,
  pageSize: 10,
};

const putWait = wait => put({ type: 'AUTH_WAIT', auth: { wait } });
const putAlert = (message, variant = 'error') => put({
  type: 'ALERT',
  alert: {
    message,
    open: true,
    variant
  }
});

async function getUserRole(firebaseApp, user) {
  const ref = firebaseApp.firestore().collection('users');
  let role = '';
  for (const p of user.providerData) {
    const fUser = ref.doc(p.uid);
    await fUser.set({
      provider: p.providerId,
      loggedAt: firebaseApp.firebase_.firestore.FieldValue.serverTimestamp(),
      displayName: p.displayName,
      email: p.email,
      emailVerified: user.emailVerified,
      phoneNumber: p.phoneNumber,
      photoURL: p.photoURL,
    }, { merge: true });
    const u = (await fUser.get()).data();
    if (u.role) role = u.role;
  }
  return role;
}

async function getUserMenu(db, user) {
  const userMenu = [];
  const { docs } = await db.collection('pages').get();
  for (const doc of docs) {
    const m = doc.data();
    if (m.role && user.role !== m.role) continue;
    m.deepSlug = m.slug;
    m.slug = `user/${m.slug}`;
    m.menu = (await db.collection(`${doc.ref.path}/menu`).get()).docs[0].data();
    userMenu.push(m);
  }
  return userMenu;
}

async function nav(path) {
  log('saga', 'nav from:', window.location.pathname, 'to:', path);
  return navigate(path, {
    state: {
      saga: true
    },
    replace: true
  });
}

// redacted

export function* authFlow() {
  try {
    const firebaseApp = yield call(readyFirebase);
    yield call(readyNavigate);
    const fbAuth = firebaseApp.auth();
    const db = firebaseApp.firestore();
    const chan = yield call(() => eventChannel(emit => fbAuth.onAuthStateChanged(user => emit({ user }))));
    while (true) {
      const { user } = yield take(chan);
      log('saga', 'user', user);
      if (user) {
        if (/login/.test(window.location.pathname)) yield call(nav, '/user/profile/');
        yield putWait(true);
        const auth = {
          user,
          loggedIn: true,
          admin: {},
          wait: false,
        };
        let adminTask;
        auth.userRole = user.role = yield call(getUserRole, firebaseApp, user);
        auth.userMenu = yield call(getUserMenu, db, user); // yield call(sleep, 1500);
        if (auth.userRole === 'admin') {
          // redacted
        } else {
          yield put({
            type: 'AUTH_LOGIN',
            auth,
          });
        }
        yield take('USER_SIGNOUT');
        yield putWait(true);
        yield call([fbAuth, fbAuth.signOut]); // yield call(sleep, 1500);
        if (auth.userRole === 'admin') adminTask.cancel();
        log('saga', 'signed out');
        continue;
      }
      // yield call(sleep, 1000);
      if (/^\/user\b/.test(window.location.pathname)) yield call(nav, '/login/');
      log('saga', 'will clear');
      yield put({ type: 'AUTH_CLEAR' });
    }
  } catch(ex) {
    console.error(ex);
    yield putAlert(ex.message);
  }
  finally {
    if (yield cancelled()) {
      console.error('cancelled');
      yield put({type: 'AUTH_CANCELLED'})
    }
  }
}
