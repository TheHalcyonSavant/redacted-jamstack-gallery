export default theme => ({
  '@global': {
    '.centered-container > .MuiCard-root': {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      margin: theme.spacing(12, 0),
      backgroundColor: theme.palette.primary.main,
    }
  }
});
