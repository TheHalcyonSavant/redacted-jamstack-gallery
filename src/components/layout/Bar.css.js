export default theme => ({
  menuButton: {
    zIndex: 1,
  },
  menu: {
    width: 200,
  },
  drawerBackButton: {
    float: 'right',
    padding: '1.2rem 1rem 1.2rem 1.5rem',
  },
  logo: {
    backgroundColor: 'rgba(255, 255, 25, 0.1)',
    padding: '5px',
    borderRadius: '10px',
    [theme.breakpoints.down('sm')]: {
      display: 'none'
    }
  },
  company: {
    zIndex: 1,
    marginLeft: '0.5em',
    fontStyle: 'italic',
    '-webkit-text-stroke': '1px #597e1b',
    // textShadow: '2px 2px #597e1b',
    [theme.breakpoints.down('sm')]: {
      display: 'none'
    }
  },
  header: {
    position: 'absolute',
    width: '100%',
    textAlign: 'center',
  },
});
