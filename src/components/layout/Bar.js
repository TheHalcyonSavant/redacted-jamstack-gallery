import React from 'react'
import { connect } from 'react-redux';
import { Link } from 'gatsby';
import { withStyles } from '@material-ui/core/styles'
import {
  AppBar,
  IconButton,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  SwipeableDrawer,
  Toolbar,
  Typography,
} from '@material-ui/core';
import * as icons from '@material-ui/icons';
import UserButton from './UserButton';
import { actions } from '../../states/connectArgs';
import styles from './Bar.css.js';

export default connect(state => ({
  userMenu: state.auth.userMenu,
}), actions)(withStyles(styles)(({ classes, currentPath, pages, markdowns, userMenu }) => {
  const [drawerState, toggleDrawer] = React.useState(false);
  const close = () => toggleDrawer(false);
  const open = () => toggleDrawer(true);
  const md = markdowns.find(m => ~m.node.fields.slug.indexOf(currentPath));
  let menus = pages;
  if (userMenu.length) menus = pages.concat(userMenu);
  menus = menus.filter(m => !m.menu.hidden).sort((a, b) => {
    const v1 = a.menu.order || 0;
    const v2 = b.menu.order || 0;
    if (v1 > v2) return -1;
    if (v1 < v2) return 1;
    return 0;
  });
  return (
    <AppBar position="sticky">
      <Toolbar disableGutters>
        <IconButton aria-label="Menu" className={classes.menuButton} onClick={open}>
          <icons.Menu />
        </IconButton>
        <img src="/logo-v1.png" alt="logo" className={classes.logo} />{/* TODO: can be gatsby-image instead */}
        <Typography variant="h6" className={classes.company}>
          SaaS Prototype &reg;
        </Typography>
        <Typography variant="h6" className={classes.header}>
          {(currentPath === 'login' && 'Login')
            || (currentPath === 'user/profile' && 'Profile')
            || (menus.find(p => (p.slug || '') === currentPath) || {}).title
            || (md && md.node.frontmatter.title)}
        </Typography>
        <UserButton />
      </Toolbar>
      <SwipeableDrawer
        open={drawerState}
        onClose={close}
        onOpen={open}
        classes={{ paper: classes.menu }}>
        <div>
          <IconButton onClick={close} className={classes.drawerBackButton}>
            <icons.ArrowBackIos />
          </IconButton>
        </div>
        <List>
          {menus.map(({ slug, menu: { muiIcon, name } }, i) => {
            const Icon = icons[muiIcon];
            if (!Icon) {
              console.error(`Icon with class name '${muiIcon}' does not exist`);
              return null;
            }
            slug = slug || '';
            return (
              <ListItem
                key={`menu-${i}`}
                button selected={currentPath === slug}
                component={Link} to={`/${slug}`} onClick={close}>
                <ListItemIcon><Icon /></ListItemIcon>
                <ListItemText primary={name} />
              </ListItem>
            );
          })}
        </List>
      </SwipeableDrawer>
    </AppBar>
  );
}));
