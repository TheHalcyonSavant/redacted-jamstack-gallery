import React from 'react';
import TestRenderer from 'react-test-renderer';
import Bar from './Bar';

jest.mock('react-redux', () => ({
  connect: () => c => c
}));
jest.mock('./UserButton');

it('have Menu button', () => {
  const props = {
    classes: {},
    currentPath: 'index',
    pages: [{
      slug: 'index',
      title: 'Index',
      menu: {
        name: 'Index',
        muiIcon: 'Home'
      }
    }, {
      slug: 'about',
      title: 'About',
      menu: {
        name: 'About',
        muiIcon: 'Help'
      }
    }],
    markdowns: [],
    userMenu: []
  };
  const r = TestRenderer.create(<Bar {...props} />);
  // MaterialUI can be tested only with Enzyme, but the simulation is buggy.
  expect(r.root.findByProps({ 'aria-label': 'Menu' })).toBeTruthy();
});
