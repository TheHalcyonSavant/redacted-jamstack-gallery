import React from 'react'
import { graphql } from 'gatsby'
import { Container, Typography, withTheme } from '@material-ui/core';

function replaceVars(html) {
  const vars = {
    '@website@': 'https://hazel-cedar-540.firebaseapp.com/',
    '@company@': `Deni Kotsev`,
    '@email@': 'thehalcyonsavant@gmail.com',
  };
  Object.keys(vars).forEach(key => html = html.replace(new RegExp(key, 'g'), vars[key]));
  return html;
}

export const pageQuery = graphql`
  query($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      frontmatter {
        date(formatString: "MMMM DD, YYYY")
        title
      }
    }
  }
`;

export default withTheme(({
  theme,
  data: {
    markdownRemark: { frontmatter: { date }, html }
  }
}) => (
  <Container style={{ padding: theme.spacing(4) }}>
    <Typography variant="subtitle2">{date}</Typography>
    <div dangerouslySetInnerHTML={{ __html: replaceVars(html) }} />
  </Container>
));
