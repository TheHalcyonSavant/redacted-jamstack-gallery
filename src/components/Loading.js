import React from 'react';
import { CircularProgress, withTheme } from '@material-ui/core';

export default withTheme(({ theme }) => (
  <div style={{
    display: 'flex',
    justifyContent: 'center',
    padding: theme.spacing(15, 0),
  }}>
    <CircularProgress align="center" size={300} />
  </div>
));
