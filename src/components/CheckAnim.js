import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import styles from './CheckAnim.css.js';

// https://codepen.io/istiaktridip/pen/BZqaOd
export default withStyles(styles)(({ classes }) => (
  <div className={classes.successCheckmark}>
    <div className={classes.checkIcon}>
      <span className={`${classes.iconLine} line-tip`}></span>
      <span className={`${classes.iconLine} line-long`}></span>
      <div className={`${classes.iconCircle}`}></div>
      <div className={classes.iconFix}></div>
    </div>
  </div>
));
