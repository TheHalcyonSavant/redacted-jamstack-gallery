import React from 'react';
import ReactPlayer from 'react-player'
import { withStyles } from '@material-ui/styles';
import styles from './VideoPlayer.css';

export default withStyles(styles)(({ classes, ...props }) => {
  // Responsive ReactPlayer
  return (
    <div className={classes.wrapper}>
      <ReactPlayer className={classes.player} width='100%' height='100%' {...props} />
    </div>
  );
});
