import React from 'react';
import { connect } from 'react-redux';
// import moment from 'moment'
import { withStyles } from '@material-ui/core/styles';
import {
  Card,
  CardContent
} from '@material-ui/core';
import ProtectRoute from '../components/ProtectRoute';
import styles from '../pages.css/Profile.css';
import { log } from '../utils';

// const time = t => moment(Number(t)).fromNow()

export default ProtectRoute()(connect(state => ({
  user: state.auth.user,
}))(withStyles(styles)(({ classes, user }) => {
  log('Profile', user);
  return (
    <div className="centered-container">
      <Card className={classes.card} raised>
        {user.photoURL && <img src={user.photoURL} alt="avatar" />}
        <CardContent component="dl">
          <dt>Display name:</dt>
          <dd>{user.isAnonymous ? 'ANONYMOUS' : user.displayName}</dd>
          {user.email && <React.Fragment>
            <dt>E-mail:</dt>
            <dd>{user.email} ({user.emailVerified ? 'VERIFIED' : 'NON-VERIFIED'})</dd>
          </React.Fragment>}
          {user.providerData.length ? <React.Fragment>
            <dt>Providers:</dt>
            <dd>{user.providerData.map(p => p.providerId).join(', ')}</dd>
          </React.Fragment> : null}
          <dt>Created:</dt>
          <dd>{user.metadata.creationTime}</dd>
          <dt>Last visit:</dt>
          <dd>{user.metadata.lastSignInTime}</dd>
          {/* <dt>Expiring:</dt>
          <dd>{time(user.stsTokenManager.expirationTime)}</dd> */}
          <dt>Phone:</dt>
          <dd>{user.phoneNumber}</dd>
        </CardContent>
      </Card>
    </div>
  )
})));
