
export function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

export async function readyNavigate() {
  while (typeof window.___navigate !== 'function') await sleep(10);
}

export function log(...args) {
  if (process.env.GATSBY_DEBUG === 'false' || process.env.NODE_ENV === 'production') return;
  console.log(...args);
}
