import React from 'react';
import { graphql } from 'gatsby';
import Img from 'gatsby-image';
import { ParallaxProvider, ParallaxBanner } from 'react-scroll-parallax';
import withStyles from '@material-ui/core/styles/withStyles';
import Link from '../components/Link';
import styles from '../pages.css/index.css';

const textContent = []; // redacted;

export const query = graphql`
  fragment VideoFields on FileConnection {
    edges {
      node {
        ext
        name
        relativePath
        publicURL
      }
    }
  }
  query {
    images: allFile(
      filter: {
        sourceInstanceName: { eq: "images" },
        relativeDirectory: { eq: "index" },
        ext: { eq: ".jpg" }
      },
      sort: { fields: name }
    ) {
      edges {
        node {
          childImageSharp {
            fluid(
              maxWidth: 2000,
              maxHeight: 800,
              # redacted
            ) {
              ...GatsbyImageSharpFluid_withWebp_tracedSVG
            }
          }
          name
        }
      }
    }
    mp4: allFile(
      filter: {
        sourceInstanceName: { eq: "videos" },
        ext: { eq: ".mp4" }
      },
      sort: { fields: name }
    ) { ...VideoFields }
    posters: allFile(
      filter: {
        sourceInstanceName: { eq: "videos" },
        ext: { eq: ".jpg" }
      },
      sort: { fields: name }
    ) { ...VideoFields }
  }
`;

export default withStyles(styles)(({ classes, data }) => {
  const filterMacOS = d => !/MACOSX/.test(d.node.relativePath);
  const mapPaths = d => d.node.publicURL;
  const mp4 = data.mp4.edges.filter(filterMacOS).map(mapPaths);
  const posters = data.posters.edges.filter(filterMacOS).map(mapPaths);
  const texts = textContent.slice();
  return (
    <ParallaxProvider>
      {data.images.edges.map((n, i) => {
        const { childImageSharp: { fluid }, name } = n.node;
        const m = name.split('-');
        let alt = m[0];
        if (m.length === 4) alt += ` ${m[1]}`;
        return (
          <React.Fragment key={`para-${i}`}>
            <ParallaxBanner layers={[{
              children: <Img
                fluid={fluid}
                style={{ height: '175vh' }}
                alt={alt}
              />,
              amount: -0.35,
            }]} style={{ height: '95vh'}}>
              <div className={classes.parallaxFrame}>
                <h1 className={classes.imageText}>{texts.shift()}</h1>
              </div>
            </ParallaxBanner>
            {mp4[i] && <ParallaxBanner className={classes.parallaxVideo} layers={[{
              amount: 0.8,
              children: <video
                className={classes.video}
                autoPlay
                loop
                playsInline
                preload="auto"
                poster={posters[i]}
                muted>
                {/* <source src={webm[i]} type="video/webm" /> */}
                <source src={mp4[i]} type="video/mp4" />
              </video>,
            }]}>
              <div className={classes.parallaxFrame}>
                <h1 className={classes.videoText}>{texts.shift()}</h1>
              </div>
            </ParallaxBanner>}
          </React.Fragment>
        )})
      }
    </ParallaxProvider>
  )
});
