import React from 'react';
import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';
import { connect } from 'react-redux';
import { Card, withTheme } from '@material-ui/core';
import { states, actions } from '../states/connectArgs';
import Loading from '../components/Loading';
import Notification from '../components/Notification';
import { firebaseApp, signInOptions } from '../firebase';
import { log } from '../utils';
import './login.module.css';

function Login({ theme, wait }) {
  const [progress, setProgress] = React.useState(true);
  log('login', `wait = ${wait}`, `progress = ${progress}`);
  const uiConfig = {
    // signInFlow: 'popup',
    signInOptions,
    callbacks: {
      signInSuccessWithAuthResult: () => false,
    },
  };
  React.useEffect(() => {
    const fbUiContainer = document.getElementById('firebaseui_container');
    if (!fbUiContainer) return;
    const stopLoader = () => {
      fbUiContainer.style.display = 'block';
      setProgress(false);
    };
    fbUiContainer.style.display = 'none';
    if (fbUiContainer.children[0] && fbUiContainer.children[0].children.length === 2) {
      stopLoader();
      return;
    }
    const observer = new MutationObserver(mutationsList => {
      if (!mutationsList[0].target || !mutationsList[0].target.children.length) return;
      const dom = mutationsList[0].target.children[0].children.length;
      if (dom === 2) {
        stopLoader();
        observer.disconnect();
      }
    });
    observer.observe(fbUiContainer, { childList: true });
  }, []);
  return (
    <div className="centered-container">
      { // when wait=false, firebaseApp is available
        !wait && <Card style={{ margin: progress ? 0 : theme.spacing(12, 0) }}><StyledFirebaseAuth
        // className={styles.firebaseUi}
        uiConfig={uiConfig}
        uiCallback={ui => {
          // ui.isPendingRedirect()
        }}
        firebaseAuth={firebaseApp && firebaseApp.auth()}
      /></Card>
      }
      <Notification />
      {progress && <Loading />}
    </div>
  );
}

export default connect(states, actions)(withTheme(Login));
