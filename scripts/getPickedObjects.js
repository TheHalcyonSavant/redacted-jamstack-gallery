var schema = require('../schema.json');

console.log(JSON.stringify(schema.__schema.types
  .filter(t => ['OBJECT', 'ENUM'].includes(t.kind)
    && !['Input', 'Connection','Aggregate','PreviousValues','Subscription','Mutation','Query'].some(f => t.name.indexOf(f) > -1)
    && t.description === null).map(t => `${t.name} - ${t.kind}`),
  null, 2));
